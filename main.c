#include <stdio.h>
#include <stdlib.h>
int airerec(int longueur, int largeur)
{
    return longueur*largeur;
}
int perimetrerec(int longueur, int largeur)
{
    return longueur*2+largeur*2;
}
int airecar(int cote)
{
    return cote*cote;
}
int perimetrecar(int cote)
{
    return cote*4;
}
int airecer(int rayon, int pi)
{
    return rayon*rayon*pi;
}
int perimetrecer(int rayon, int pi)
{
    return 2*rayon*pi;
}

int main(int argc, char *argv[])
{
    int longueur=0;
    int largeur=0;
    int cote=0;
    int rayon=0;
    int pi=3.1416;
int choixmenu;

printf("Choisi une figure\n");
printf("1. Rectangle\n");
printf("2. Carre\n");
printf("3. Cercle\n");
scanf("%d", &choixmenu);

printf("\n");

switch(choixmenu)
{
 case 1:
     printf("Tu as choisi le rectangle\n\n");
     int choixrec;
     printf("Choisi le calcul a effectuer\n");
     printf("1. Aire\n");
     printf("2. Perimetre\n");
     scanf("%d", &choixrec);
     printf("\n");
     switch(choixrec)
     {
     case 1:
        printf("Tu as choisi de calculer l'aire\n\n");
        printf("Saisir une longueur\n");
        scanf("%d", &longueur);
        printf("Saisir une largeur\n");
        scanf("%d", &largeur);
        int v_airerec=airerec(longueur,largeur);
        printf("l'aire du rectangle est %d\n", v_airerec);
        break;
     case 2:
         printf("Tu as choisi de calculer le perimetre\n\n");
         printf("Saisir une longueur\n");
         scanf("%d", &longueur);
         printf("Saisir une largeur\n");
         scanf("%d", &largeur);
         int v_perimetrerec=perimetrerec(longueur,largeur);
         printf("le perimetre du rectangle est %d\n", v_perimetrerec);
        break;
     }

     break;
 case 2:
    printf("Tu as choisi le carre\n\n");
    int choixcar;
    printf("Choisi le calcul a effectuer\n");
    printf("1. Aire\n");
    printf("2. Perimetre\n");
    scanf("%d", &choixcar);
    printf("\n");
    switch(choixcar)
    {
    case 1:
        printf("Tu as choisi de calculer l'aire\n\n");
        printf("Saisir la valeure d'un cote\n");
        scanf("%d", &cote);
        int v_airecar=airecar(cote);
        printf("l'aire du carre est %d\n", v_airecar);
        break;
    case 2:
        printf("Tu as choisi de calculer le perimetre\n\n");
        printf("Saisir la valeure d'un cote\n");
        scanf("%d", &cote);
        int v_perimetrecar=perimetrecar(cote);
        printf("le perimetre du carre est %d\n", v_perimetrecar);
        break;
    }
    break;
 case 3:
    printf("Tu as choisi le cercle\n\n");
    int choixcer;
    printf("Choisi le calcul a effectuer\n");
    printf("1. Aire\n");
    printf("2. Perimetre\n");
    scanf("%d", &choixcer);
    printf("\n");
    switch(choixcer)
    {
    case 1:
        printf("Tu as choisi de calculer l'aire\n\n");
        printf("Saisir un rayon\n");
        scanf("%d", &rayon);
        int v_airecer=airecer(rayon,pi);
        printf("l'aire du cercle est %d\n", v_airecer);
        break;
    case 2:
        printf("Tu as choisi de calculer le perimetre\n\n");
        printf("Saisir un rayon\n");
        scanf("%d", &rayon);
        int v_perimetrecer=perimetrecer(rayon,pi);
        printf("le perimetre du cercle est %d\n", v_perimetrecer);
        break;
    }
    break;


}
printf("\n");
    return 0;
}
